const commander = require('commander');

const mcPixels = require('./mc-pixels');
const pkg = require('../package.json');

const defaultAction = (nodeExec, jsFile, imageFile, cmd) => {
  if (!cmd) {
    throw Error('imageFile is required');
  }

  return mcPixels.render(imageFile)
    .catch(err => {
      console.error('Error in minecraft:', err.stack || err); // eslint-disable-line no-console
    });
};

const startCommandLine = () => {
  const cmd = new commander.Command(pkg.name).version(pkg.version);

  cmd.action(defaultAction);

  cmd.parseArgs(process.argv);
};

module.exports = { startCommandLine };
