const mcpi = require('mcpi');

const denodify = (fn, ctx) => function () { // eslint-disable-line func-names
  const args = Array.apply(null, arguments); // eslint-disable-line prefer-rest-params
  return new Promise((resolve, reject) => {
    args.push((err, result) => {
      if (err) {
        reject(err);
        return;
      }

      resolve(result);
    });

    fn.apply(ctx, args);
  });
};

const getPixels = denodify(require('get-pixels'));

const createColorValue = (mcColor, r, g, b) => ({ mcColor, r, g, b });

const colorInfoTableWool = [
  createColorValue(mcpi.Colors.White, 235, 235, 235),
  createColorValue(mcpi.Colors.Orange, 217, 127, 67),
  createColorValue(mcpi.Colors.Magenta, 172, 165, 181),
  createColorValue(mcpi.Colors.LightBlue, 111, 141, 201),
  createColorValue(mcpi.Colors.Yellow, 168, 157, 34),
  createColorValue(mcpi.Colors.Lime, 64, 174, 54),
  createColorValue(mcpi.Colors.Pink, 212, 148, 165),
  createColorValue(mcpi.Colors.Gray, 62, 62, 62),
  createColorValue(mcpi.Colors.LightGray, 170, 170, 170),
  createColorValue(mcpi.Colors.Cyan, 45, 110, 139),
  createColorValue(mcpi.Colors.Purple, 142, 79, 195),
  createColorValue(mcpi.Colors.Blue, 42, 53, 136),
  createColorValue(mcpi.Colors.Brown, 73, 45, 27),
  createColorValue(mcpi.Colors.Green, 52, 71, 26),
  createColorValue(mcpi.Colors.Red, 162, 55, 55),
  createColorValue(mcpi.Colors.Black, 24, 24, 24),
];

const convertToColor = pixel => {
  const diffs = colorInfoTableWool.map(({ mcColor, r, g, b }) =>
    ({ mcColor, diff: Math.pow(pixel.r - r, 2) + Math.pow(pixel.g - g, 2) + Math.pow(pixel.b - b, 2) }));
  const sortedDiffs = diffs.sort((a, b) => a.diff - b.diff);
  const chosenDiff = sortedDiffs[0];
  return chosenDiff.mcColor;
};

const averagePixels = pixels => {
  const totalPixel = pixels.reduce((current, next) => ({
    r: current.r + next.r,
    g: current.g + next.g,
    b: current.b + next.b,
  }), { r: 0, g: 0, b: 0 });

  return {
    r: totalPixel.r / pixels.length,
    g: totalPixel.g / pixels.length,
    b: totalPixel.b / pixels.length,
  };
};

function renderSpaceAroundImage(mc, pos, imageWidth, imageHeight, numSpaceBlocks) {
  const front = mc.world.setBlocks(pos.x - imageWidth / 2, pos.y, pos.z - 1,
    pos.x + imageWidth / 2, pos.y + imageHeight, pos.z - numSpaceBlocks - 1, mcpi.Blocks.Air);
  const back = mc.world.setBlocks(pos.x - imageWidth / 2, pos.y, pos.z + 1,
    pos.x + imageWidth / 2, pos.y + imageHeight, pos.z + numSpaceBlocks - 1, mcpi.Blocks.Air);

  return Promise.all([front, back]);
}

const render = (imageFile) => getPixels(imageFile)
  .then(pixels => {
    const mc = new mcpi.Minecraft('localhost', 4711);
    return mc.player.getTilePos().then(pos => {
      const nx = pixels.shape[0];
      const ny = pixels.shape[1];
      const nc = pixels.shape[2];

      if (nc < 3) {
        throw Error('Unknown graphic has too few color values:', nc);
      }

      // console.log(`Width: ${nx}, Height: ${ny}, Color Depth: ${nc}`);

      const maxHeight = 100;
      const divisor = Math.ceil(ny / maxHeight);

      for (let i = 0, x = pos.x - nx / 2 / divisor; i < nx; i += divisor, x++) {
        for (let j = 0, y = pos.y + ny / divisor; j < ny; j += divisor, y--) {
          const pixelsToAverage = [];
          for (let iOffset = 0; iOffset < divisor; iOffset++) {
            for (let jOffset = 0; jOffset < divisor; jOffset++) {
              pixelsToAverage.push({
                r: pixels.get(i + iOffset, j + jOffset, 0),
                g: pixels.get(i + iOffset, j + jOffset, 1),
                b: pixels.get(i + iOffset, j + jOffset, 2),
              });
            }
          }

          const pixel = averagePixels(pixelsToAverage);

          const color = convertToColor(pixel);
          mc.world.setBlock(x, y, pos.z, mcpi.Blocks.Wool, color);
        }
      }

      return renderSpaceAroundImage(mc, pos, nx / divisor, ny / divisor, 10);
    })
    .then(() => mc.close())
    .catch(err => {
      mc.close();
      throw err;
    });
  });

module.exports = { render };
